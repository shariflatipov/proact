from rest_framework.views import exception_handler


def custom_exception_handler(exception, context):
    """
    Custom exception handler for Django Rest Framework that adds
    the `status_code` to the response and renames the `detail` key to `error`.
    """
    response = exception_handler(exception, context)

    if response is not None:
        response.data['status_code'] = response.status_code
        if 'detail' in response.data:
            response.data['status'] = response.data['detail']
            del response.data['detail']
        if 'non_field_errors' in response.data:
            response.data['status'] = response.data['non_field_errors']
            del response.data['non_field_errors']

    return response
