from django.contrib import admin

from base.models import (
    Country,
    Region,
    District,
    Sector
)

admin.site.register(Country)
admin.site.register(Region)
admin.site.register(District)
admin.site.register(Sector)
