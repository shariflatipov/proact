from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView, DeleteView

from base.models import Country, Region, District, Sector


def index(request):
    return render(request, 'base/index.html')


class CountryList(ListView):
    model = Country


class CountryCreate(CreateView):
    model = Country
    fields = '__all__'
    success_url = reverse_lazy('base:country_list')


class CountryDelete(DeleteView):
    model = Country
    success_url = reverse_lazy('base:country_list')

class CountryUpdate(UpdateView):
    model = Country
    fields = '__all__'
    success_url = reverse_lazy('base:country_list')


class DistrictCreate(CreateView):
    model = District
    fields = '__all__'
    success_url = reverse_lazy('base:district_list')


class DistrictDelete(DeleteView):
    model = District 
    success_url = reverse_lazy('base:district_list')

class DistrictUpdate(UpdateView):
    model = District
    fields = '__all__'
    success_url = reverse_lazy('base:district_list')


class DistrictList(ListView):
    model = District


class RegionList(ListView):
    model = Region


class RegionCreate(CreateView):
    model = Region
    fields = '__all__'
    success_url = reverse_lazy('base:region_list')


class RegionDelete(DeleteView):
    model = Region
    success_url = reverse_lazy('base:region_list')

class RegionUpdate(UpdateView):
    model = Region
    fields = '__all__'
    success_url = reverse_lazy('base:region_list')


class SectorList(ListView):
    model = Sector


class SectorCreate(CreateView):
    model = Sector
    fields = '__all__'
    success_url = reverse_lazy('base:sector_list')


class SectorDelete(DeleteView):
    model = Sector
    success_url = reverse_lazy('base:sector_list')

class SectorUpdate(UpdateView):
    model = Sector
    fields = '__all__'
    success_url = reverse_lazy('base:sector_list')
