from django.conf.urls import url

from base import api_views as views

app_name = 'base'

urlpatterns = [
    url(r'^country/list/$', views.CountryList.as_view(), name='country_list'),
    url(r'^country/update/(?P<pk>\d+)/$', views.CountryUpdate.as_view(), name='country_update'),
    url(r'^country/create/$', views.CountryCreate.as_view(), name='country_create'),

    url(r'^region/list/$', views.RegionList.as_view(), name='region_list'),
    url(r'^region/update/(?P<pk>\d+)/$', views.RegionUpdate.as_view(), name='region_update'),
    url(r'^region/create/$', views.RegionCreate.as_view(), name='region_create'),

    url(r'^district/list/$', views.DistrictList.as_view(), name='district_list'),
    url(r'^district/update/(?P<pk>\d+)/$', views.DistrictUpdate.as_view(), name='district_update'),
    url(r'^district/create/$', views.DistrictCreate.as_view(), name='district_create'),

    url(r'^sector/list/$', views.SectorList.as_view(), name='sector_list'),
    url(r'^sector/update/(?P<pk>\d+)/$', views.SectorUpdate.as_view(), name='sector_update'),
    url(r'^sector/create/$', views.SectorCreate.as_view(), name='sector_create'),
]
