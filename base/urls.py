from django.conf.urls import url
from django.urls import path

from base import views

app_name = 'base'

urlpatterns = [
    path('country/list/', views.CountryList.as_view(), name='country_list'),
    path('country/create/', views.CountryCreate.as_view(), name='country_create'),
    path('country/update/<int:pk>', views.CountryUpdate.as_view(), name='country_update'),
    path('country/delete/<int:pk>', views.CountryDelete.as_view(), name='country_delete'),

    path('district/list/', views.DistrictList.as_view(), name='district_list'),
    path('district/create/', views.DistrictCreate.as_view(), name='district_create'),
    path('district/update/<int:pk>', views.DistrictUpdate.as_view(), name='district_update'),
    path('district/delete/<int:pk>', views.DistrictDelete.as_view(), name='district_delete'),

    path('region/list/', views.RegionList.as_view(), name='region_list'),
    path('region/create/', views.RegionCreate.as_view(), name='region_create'),
    path('region/update/<int:pk>', views.RegionUpdate.as_view(), name='region_update'),
    path('region/delete/<int:pk>', views.RegionDelete.as_view(), name='region_delete'),

    path('sector/list/', views.SectorList.as_view(), name='sector_list'),
    path('sector/create/', views.SectorCreate.as_view(), name='sector_create'),
    path('sector/update/<int:pk>', views.SectorUpdate.as_view(), name='sector_update'),
    path('sector/delete/<int:pk>', views.SectorDelete.as_view(), name='sector_delete'),

]
