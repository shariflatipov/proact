from django.shortcuts import render
from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView

from base.models import Country, Region, District, Sector

from base.serializers import (
    CountrySerializer, RegionSerializer,
    DistrictSerializer, SectorSerializer
)


class CountryCreate(CreateAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class CountryList(ListAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class CountryUpdate(UpdateAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class RegionCreate(CreateAPIView):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer


class RegionUpdate(UpdateAPIView):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer


class RegionList(ListAPIView):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer


class DistrictCreate(CreateAPIView):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer


class DistrictUpdate(UpdateAPIView):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer


class DistrictList(ListAPIView):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer


class SectorList(ListAPIView):
    queryset = Sector.objects.all()
    serializer_class = SectorSerializer


class SectorUpdate(UpdateAPIView):
    queryset = Sector.objects.all()
    serializer_class = SectorSerializer


class SectorCreate(CreateAPIView):
    queryset = Sector.objects.all()
    serializer_class = SectorSerializer
