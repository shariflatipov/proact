from rest_framework import renderers

class CustomRenderer(renderers.JSONRenderer):
    charset = 'utf-8'
