from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView

from projects.models import Project, Partner, Employee, EmployeeType

from projects.serializers import ProjectSerializer, PartnerSerializer, EmployeeSerializer, EmployeeTypeSerializer


class ProjectCreateApiView(CreateAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    

class ProjectListApiView(ListAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    def get_queryset(self):
        qs = super(ProjectListApiView, self).get_queryset()
        return qs.filter(owner=self.request.user)


class PartnerCreate(CreateAPIView):
    queryset = Partner.objects.all()
    serializer_class = PartnerSerializer


class PartnerUpdate(UpdateAPIView):
    queryset = Partner.objects.all()
    serializer_class = PartnerSerializer


class PartnerList(ListAPIView):
    queryset = Partner.objects.all()
    serializer_class = PartnerSerializer


class EmployeeCreate(CreateAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer


class EmployeeUpdate(UpdateAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer


class EmployeeList(ListAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer


class EmployeeTypeCreate(CreateAPIView):
    queryset = EmployeeType.objects.all()
    serializer_class = EmployeeTypeSerializer


class EmployeeTypeUpdate(UpdateAPIView):
    queryset = EmployeeType.objects.all()
    serializer_class = EmployeeTypeSerializer


class EmployeeTypeList(ListAPIView):
    queryset = EmployeeType.objects.all()
    serializer_class = EmployeeTypeSerializer
