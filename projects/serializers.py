from rest_framework import serializers
from projects.models import Project, Partner, Employee, EmployeeType


class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = '__all__'


class PartnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Partner
        fields = '__all__'


class EmployeeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Employee
        fields = '__all__'


class EmployeeTypeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = EmployeeType
        fields = '__all__'
