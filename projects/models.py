from django.contrib.auth.models import User
from django.db import models

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

from base.models import District, Sector


class CustomObtainToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, 'user': {'id': user.pk, 'name': user.get_full_name()},
                         'status': {'code':1, 'msg': 'Пользователь успешно залогинился'}})

FEMALE = 0
MALE = 1

GENDER = ((MALE, 'Мужской'), (FEMALE, 'Женский'))

class Partner(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Staff(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE)


class EmployeeType(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=200)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    partner_contribution = models.FloatField()
    own_contribution = models.FloatField()
    other_contribution = models.FloatField()
    sector = models.ForeignKey(Sector, on_delete=models.CASCADE)
    establish_year = models.SmallIntegerField()
    contact_phone = models.CharField(max_length=255, blank=True, null=True)
    contact_email = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name


class Employee(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    date_of_birth = models.DateField()
    gender = models.PositiveSmallIntegerField(choices=GENDER)
    employee_type = models.ForeignKey(EmployeeType, on_delete=models.CASCADE)
    start_date = models.DateField(auto_now=True)
    end_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.name
