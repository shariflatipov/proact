from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.db.models import Q

from projects.models import Project, EmployeeType, Employee, Partner


class ProjectCreate(CreateView):
    model = Project
    fields = '__all__'
    success_url = reverse_lazy('projects:project_list')


class ProjectList(ListView):
    model = Project
    

def project_details(request, pk):
    project = get_object_or_404(Project, pk=pk)
    ctx = {'project': project}
    return render(request, 'projects/project_details.html', ctx)


class EmployeeTypeCreate(CreateView):
    model = EmployeeType
    fields = '__all__'
    success_url = reverse_lazy('projects:employeetype_list')


class EmployeeTypeList(ListView):
    model = EmployeeType


def employeetype_details(request, pk):
    employee_type = get_object_or_404(EmployeeType, pk=pk)
    ctx = {'employee_type': employee_type}
    return render(request, 'projects/employeetype_details.html', ctx)


class EmployeeTypeDelete(DeleteView):
    model = EmployeeType
    success_url = reverse_lazy('projects:employeetype_list')


class EmployeeTypeUpdate(UpdateView):
    model = EmployeeType
    fields = '__all__'
    success_url = reverse_lazy('projects:employeetype_list')


class EmployeeCreate(CreateView):
    model = Employee
    fields = ('project', 'name', 'date_of_birth', 'gender', 'employee_type')
    success_url = reverse_lazy('projects:employee_list')


class EmployeeDelete(DeleteView):
    model = Employee
    success_url = reverse_lazy('projects:employee_list')



class EmployeeList(ListView):
    model = Employee


class EmployeeUpdate(UpdateView):
    model = Employee
    fields = ('project', 'name', 'date_of_birth', 'gender', 'employee_type')
    success_url = reverse_lazy('projects:employee_list')


def employee_details(request, pk):
    employee_type = get_object_or_404(Employee, pk=pk)
    ctx = {'employee': employee}
    return render(request, 'projects/employee_details.html', ctx)


class PartnerCreate(CreateView):
    model = Partner
    fields = '__all__'
    success_url = reverse_lazy('projects:partner_list')


class PartnerUpdate(UpdateView):
    model = Partner
    fields = '__all__'
    success_url = reverse_lazy('projects:partner_list')


class PartnerList(ListView):
    model = Partner


def partner_details(request, pk):
    partner = get_object_or_404(Partner, pk=pk)
    ctx = {'partner': partner}
    return render(request, 'projects/partner_details.html', ctx)


class PartnerDelete(DeleteView):
    model = Partner
    success_url = reverse_lazy('projects:partner_list')
