from django.contrib import admin

from projects.models import (
    Partner,
    Staff,
    Employee,
    EmployeeType,
    Project
)

admin.site.register(Partner)
admin.site.register(Staff)
admin.site.register(Employee)
admin.site.register(EmployeeType)
admin.site.register(Project)
