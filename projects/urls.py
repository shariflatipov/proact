from django.conf.urls import url
from django.urls import path

from projects import views

app_name = 'projects'

urlpatterns = [
    url(r'^project/create/$', views.ProjectCreate.as_view(), name='project_create'),
    url(r'^project/list/$', views.ProjectList.as_view(), name='project_list'),
    path('project/details/<int:pk>/', views.project_details, name='project_details'),

    path('employeetype/create/', views.EmployeeTypeCreate.as_view(), name='employeetype_create'),
    path('employeetype/update/<int:pk>/', views.EmployeeTypeUpdate.as_view(), name='employeetype_update'),
    path('employeetype/delete/<int:pk>/', views.EmployeeTypeDelete.as_view(), name='employeetype_delete'),
    path('employeetype/list/', views.EmployeeTypeList.as_view(), name='employeetype_list'),
    path('employeetype/details/<int:pk>/', views.employeetype_details, name='employeetype_details'),

    path('employee/create/', views.EmployeeCreate.as_view(), name='employee_create'),
    path('employee/update/<int:pk>/', views.EmployeeUpdate.as_view(), name='employee_update'),
    path('employee/delete/<int:pk>/', views.EmployeeDelete.as_view(), name='employee_delete'),
    path('employee/list/', views.EmployeeList.as_view(), name='employee_list'),
    path('employee/details/<int:pk>/', views.employee_details, name='employee_details'),

    path('partner/create/', views.PartnerCreate.as_view(), name='partner_create'),
    path('partner/update/<int:pk>/', views.PartnerUpdate.as_view(), name='partner_update'),
    path('partner/delete/<int:pk>/', views.PartnerDelete.as_view(), name='partner_delete'),
    path('partner/list/', views.PartnerList.as_view(), name='partner_list'),
    path('partner/details/<int:pk>/', views.partner_details, name='partner_details'),
]
