from django.conf.urls import url

from projects import api_views as views

app_name = 'projects'

urlpatterns = [
    url(r'^project/create/$', views.ProjectCreateApiView.as_view(), name='project_create'),
    url(r'^project/list/$', views.ProjectListApiView.as_view(), name='project_list'),

    url(r'^partner/list/$', views.PartnerList.as_view(), name='partner_list'),
    url(r'^partner/create/$', views.PartnerCreate.as_view(), name='partner_create'),
    url(r'^partner/update/(?P<pk>\d+)/$', views.PartnerUpdate.as_view(), name='partner_update'),

    url(r'^employee/list/$', views.EmployeeList.as_view(), name='employee_list'),
    url(r'^employee/create/$', views.EmployeeCreate.as_view(), name='employee_create'),
    url(r'^employee/update/(?P<pk>\d+)/$', views.EmployeeUpdate.as_view(), name='employee_update'),

    url(r'^employeetype/list/$', views.EmployeeTypeList.as_view(), name='employeetype_list'),
    url(r'^employeetype/create/$', views.EmployeeTypeCreate.as_view(), name='employeetype_create'),
    url(r'^employeetype/update/(?P<pk>\d+)/$', views.EmployeeTypeUpdate.as_view(), name='employeetype_update'),
]
