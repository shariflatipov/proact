from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, ListAPIView, UpdateAPIView

from operations.models import (
    Repayment, CostCenter, ExpenseCategory,
    Expenses, Income, IncomeCategory
)

from projects.models import Staff

from operations.serializers import (
    RepaymentSerializer, CostCenterSerializer, ExpenseSerializer,
    ExpenseCategorySerializer, IncomeSerializer, IncomeCategorySerializer
)


class RepaymentCreate(CreateAPIView):
    queryset = Repayment.objects.all()
    serializer_class = RepaymentSerializer


class RepaymentList(ListAPIView):
    queryset = Repayment.objects.all()
    serializer_class = RepaymentSerializer

    
class RepaymentUpdate(ListAPIView):
    queryset = Repayment.objects.all()
    serializer_class = RepaymentSerializer


class CostCenterCreate(CreateAPIView):
    queryset = CostCenter.objects.all()
    serializer_class = CostCenterSerializer


class CostCenterUpdate(UpdateAPIView):
    queryset = CostCenter.objects.all()
    serializer_class = CostCenterSerializer


class CostCenterList(ListAPIView):
    queryset = CostCenter.objects.all()
    serializer_class = CostCenterSerializer


class ExpenseCreate(CreateAPIView):
    queryset = Expenses.objects.all()
    serializer_class = ExpenseSerializer
    def post(self, request):
        serializer_class = self.serializer_class(data=request.data)
        data = {}
        if serializer_class.is_valid():
            vd = serializer_class.validated_data
            vd['created_by'] = Staff.objects.get(user=request.user)
            serializer_class.save()

            data = serializer_class.data
        else:
            data = serializer_class.errors
        return Response(data)



class ExpenseList(ListAPIView):
    queryset = Expenses.objects.all()
    serializer_class = ExpenseSerializer


class ExpenseUpdate(UpdateAPIView):
    queryset = Expenses.objects.all()
    serializer_class = ExpenseSerializer


class ExpenseCategoryCreate(CreateAPIView):
    queryset = ExpenseCategory.objects.all()
    serializer_class = ExpenseCategorySerializer


class ExpenseCategoryList(ListAPIView):
    queryset = ExpenseCategory.objects.all()
    serializer_class = ExpenseCategorySerializer


class ExpenseCategoryUpdate(UpdateAPIView):
    queryset = ExpenseCategory.objects.all()
    serializer_class = ExpenseCategorySerializer


class IncomeList(ListAPIView):
    queryset = Income.objects.all()
    serializer_class = IncomeSerializer


class IncomeCreate(CreateAPIView):
    queryset = Income.objects.all()
    serializer_class = IncomeSerializer

    def post(self, request):
        serializer_class = self.serializer_class(data=request.data)
        data = {}
        if serializer_class.is_valid():
            vd = serializer_class.validated_data
            vd['created_by'] = Staff.objects.get(user=request.user)
            serializer_class.save()

            data = serializer_class.data
        else:
            data = serializer_class.errors
        return Response(data)


class IncomeUpdate(UpdateAPIView):
    queryset = Income.objects.all()
    serializer_class = IncomeSerializer


class IncomeCategoryCreate(CreateAPIView):
    queryset = IncomeCategory.objects.all()
    serializer_class = IncomeCategorySerializer


class IncomeCategoryUpdate(UpdateAPIView):
    queryset = IncomeCategory.objects.all()
    serializer_class = IncomeCategorySerializer


class IncomeCategoryList(ListAPIView):
    queryset = IncomeCategory.objects.all()
    serializer_class = IncomeCategorySerializer
