from rest_framework import serializers

from operations.models import (
    Repayment, CostCenter, ExpenseCategory,
    Expenses, Income, IncomeCategory
)


class RepaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Repayment
        fields = '__all__'


class CostCenterSerializer(serializers.ModelSerializer):
    class Meta:
        model = CostCenter
        fields = '__all__'


class ExpenseCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ExpenseCategory
        fields = '__all__'


class ExpenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expenses
        exclude = ('created_by',)


class IncomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Income
        exclude = ('created_by',)


class IncomeCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = IncomeCategory
        fields = '__all__'
