from django.db import models

from projects.models import Project, Staff, Employee


class Repayment(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    date = models.DateField()
    loan_amount = models.FloatField()
    paid_amount = models.FloatField()


class CostCenter(models.Model):
    code = models.CharField(max_length=100)
    name = models.CharField(max_length=255)


class ExpenseCategory(models.Model):
    code = models.CharField(max_length=100)
    name = models.CharField(max_length=255)


class Expenses(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    created_by = models.ForeignKey(Staff, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now=True)
    amount = models.FloatField()
    cost_center = models.ForeignKey(CostCenter, on_delete=models.CASCADE)
    comment = models.TextField(blank=True, null=True)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, blank=True, null=True)
    expense_category = models.ForeignKey(ExpenseCategory, on_delete=models.CASCADE)


class IncomeCategory(models.Model):
    name = models.CharField(max_length=255)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)


class Income(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    created_by = models.ForeignKey(Staff, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now=True)
    amount = models.FloatField()
    cost_center = models.ForeignKey(CostCenter, on_delete=models.CASCADE)
    comment = models.TextField(blank=True, null=True)
    income_category = models.ForeignKey(IncomeCategory, on_delete=models.CASCADE)
