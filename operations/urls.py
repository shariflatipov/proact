from django.urls import path
from django.conf.urls import url

from operations import views

app_name = 'operations'

urlpatterns = [
    path('costcenter/list/', views.CostCenterList.as_view(), name='costcenter_list'),
    path('costcenter/create/', views.CostCenterCreate.as_view(), name='costcenter_create'),
    path('costcenter/update/<int:pk>', views.CostCenterUpdate.as_view(), name='costcenter_update'),
    path('costcenter/delete/<int:pk>', views.CostCenterDelete.as_view(), name='costcenter_delete'),

    path('repayment/list/', views.RepaymentList.as_view(), name='repayment_list'),
    path('repayment/create/', views.RepaymentCreate.as_view(), name='repayment_create'),
    path('repayment/update/<int:pk>', views.RepaymentUpdate.as_view(), name='repayment_update'),
    path('repayment/delete/<int:pk>', views.RepaymentDelete.as_view(), name='repayment_delete'),

    path('expensecategory/list/', views.ExpenseCategoryList.as_view(), name='expensecategory_list'),
    path('expensecategory/create/', views.ExpenseCategoryCreate.as_view(), name='expensecategory_create'),
    path('expensecategory/update/<int:pk>', views.ExpenseCategoryUpdate.as_view(), name='expensecategory_update'),
    path('expensecategory/delete/<int:pk>', views.ExpenseCategoryDelete.as_view(), name='expensecategory_delete'),

    path('incomecategory/list/', views.IncomeCategoryList.as_view(), name='incomecategory_list'),
    path('incomecategory/create/', views.IncomeCategoryCreate.as_view(), name='incomecategory_create'),
    path('incomecategory/update/<int:pk>', views.IncomeCategoryUpdate.as_view(), name='incomecategory_update'),
    path('incomecategory/delete/<int:pk>', views.IncomeCategoryDelete.as_view(), name='incomecategory_delete'),

    url(r'^expense/list/$', views.ExpenseList.as_view(), name='expense_list'),
    url(r'^income/list/$', views.IncomeList.as_view(), name='income_list'),
]
