from django.conf.urls import url

from operations import api_views as views

app_name = 'operations'

urlpatterns = [
    url(r'^repayment/list/$', views.RepaymentList.as_view(), name='repayment_list'),
    url(r'^repayment/create/$', views.RepaymentCreate.as_view(), name='repayment_create'),
    url(r'^repayment/update/(?P<pk>\d+)/$', views.RepaymentUpdate.as_view(), name='repayment_update'),

    url(r'^costcenter/list/$', views.CostCenterList.as_view(), name='costcenter_list'),
    url(r'^costcenter/create/$', views.CostCenterCreate.as_view(), name='costcenter_create'),
    url(r'^costcenter/update/(?P<pk>\d+)/$', views.CostCenterUpdate.as_view(), name='costcenter_update'),

    url(r'^expensecategory/list/$', views.ExpenseCategoryList.as_view(), name='expensecategory_list'),
    url(r'^expensecategory/create/$', views.ExpenseCategoryCreate.as_view(), name='expensecategory_create'),
    url(r'^expensecategory/update/(?P<pk>\d+)/$', views.ExpenseCategoryUpdate.as_view(), name='expensecategory_update'),

    url(r'^expense/list/$', views.ExpenseList.as_view(), name='expense_list'),
    url(r'^expense/create/$', views.ExpenseCreate.as_view(), name='expense_create'),
    url(r'^expense/update/(?P<pk>\d+)/$', views.ExpenseUpdate.as_view(), name='expense_update'),

    url(r'^income/list/$', views.IncomeList.as_view(), name='income_list'),
    url(r'^income/create/$', views.IncomeCreate.as_view(), name='income_create'),
    url(r'^income/update/(?P<pk>\d+)/$', views.IncomeUpdate.as_view(), name='income_update'),

    url(r'^incomecategory/list/$', views.IncomeCategoryList.as_view(), name='incomecategory_list'),
    url(r'^incomecategory/create/$', views.IncomeCategoryCreate.as_view(), name='incomecategory_create'),
    url(r'^incomecategory/update/(?P<pk>\d+)/$', views.IncomeCategoryUpdate.as_view(), name='incomecategory_update'),
]
