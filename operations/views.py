from django.views.generic import CreateView, UpdateView, ListView, DeleteView
from django.urls import reverse_lazy
from projects.models import Staff

from operations.models import (
    Repayment,
    CostCenter,
    Expenses,
    ExpenseCategory,
    Income,
    IncomeCategory
)



class RepaymentList(ListView):
    model = Repayment


class RepaymentCreate(CreateView):
    model = Repayment
    fields = '__all__'
    success_url = reverse_lazy('operations:repayment_list')


class RepaymentDelete(DeleteView):
    model = Repayment
    success_url = reverse_lazy('operations:repayment_list')


class RepaymentUpdate(UpdateView):
    model = Repayment
    fields = '__all__'
    success_url = reverse_lazy('operations:repayment_list')

    
class CostCenterList(ListView):
    model = CostCenter


class CostCenterCreate(CreateView):
    model =CostCenter
    fields = '__all__'
    success_url = reverse_lazy('operations:costcenter_list')


class CostCenterDelete(DeleteView):
    model = CostCenter
    success_url = reverse_lazy('operations:costcenter_list')


class CostCenterUpdate(UpdateView):
    model = CostCenter
    fields = '__all__'
    success_url = reverse_lazy('operations:costcenter_list')


class ExpenseList(ListView):
    model = Expenses


class ExpenseCategoryList(ListView):
    model = ExpenseCategory


class ExpenseCategoryCreate(CreateView):
    model = ExpenseCategory
    fields = '__all__'
    success_url = reverse_lazy('operations:expensecategory_list')


class ExpenseCategoryDelete(DeleteView):
    model = ExpenseCategory
    success_url = reverse_lazy('operations:expensecategory_list')


class ExpenseCategoryUpdate(UpdateView):
    model = ExpenseCategory
    fields = '__all__'
    success_url = reverse_lazy('operations:expensecategory_list')


class IncomeList(ListView):
    model = Income


class IncomeCategoryList(ListView):
    model = IncomeCategory


class IncomeCategoryCreate(CreateView):
    model = IncomeCategory
    fields = '__all__'
    success_url = reverse_lazy('operations:incomecategory_list')


class IncomeCategoryDelete(DeleteView):
    model = IncomeCategory
    success_url = reverse_lazy('operations:incomecategory_list')


class IncomeCategoryUpdate(UpdateView):
    model = IncomeCategory
    fields = '__all__'
    success_url = reverse_lazy('operations:incomecategory_list')
