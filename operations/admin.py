from django.contrib import admin

from operations.models import (
    Expenses,
    ExpenseCategory,
    Income,
    IncomeCategory,
    Repayment,
    CostCenter,
)

admin.site.register(Expenses)
admin.site.register(ExpenseCategory)
admin.site.register(Income)
admin.site.register(IncomeCategory)
admin.site.register(Repayment)
admin.site.register(CostCenter)
